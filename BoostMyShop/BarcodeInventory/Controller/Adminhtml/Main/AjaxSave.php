<?php
namespace BoostMyShop\BarcodeInventory\Controller\Adminhtml\Main;

use Magento\Framework\Controller\ResultFactory;

class AjaxSave extends \Magento\Backend\App\AbstractAction
{
    protected $resultJsonFactory;


    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
    ) {
        parent::__construct($context);
        $this->resultJsonFactory = $resultJsonFactory;
    }

    public function execute()
    {
        /** @var \Magento\Framework\Controller\Result\Json $result */
        $result = $this->resultJsonFactory->create();

        try
        {
            $id = $this->getRequest()->getParam('id');
            $qty = $this->getRequest()->getParam('qty');

            $product = $this->_objectManager->create('Magento\Catalog\Model\Product')->load($id);

            $product->setStockData(array(
                'qty' => $qty,
                'is_in_stock' => ($qty > 0) ? 1 : 0
            ));

            $product->save();

            return $result->setData(['success' => true]);
        }
        catch(\Exception $ex)
        {
            return $result->setData(['success' => false, 'msg' => $ex->getMessage()]);
        }

    }

    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Boostmyshop_Barcodeinventory::Main');
    }

}